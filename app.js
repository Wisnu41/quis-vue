Vue.component('comment', {
  template: '#comment',
  props: ['comment', 'date', 'score'],
  data: function () {
    return {
      toggle: "",
      cscore: this.score,
      add: function () {
        if (this.toggle !== "up" && this.toggle != "down") {
          this.cscore = this.cscore + 1
          this.toggle = "up"
        } else if (this.toggle == "down") {
          this.cscore = this.cscore + 2
          this.toggle = "up"
        }
        else {
          this.cscore = this.cscore - 1
          this.toggle = ""
        }
      },
      down: function () {
        if (this.toggle !== "down" && this.toggle != "up") {
          this.cscore = this.cscore - 1
          this.toggle = "down"
        } else if (this.toggle == "up") {
          this.cscore = this.cscore - 2
          this.toggle = "down"
        }
        else {
          this.cscore = this.cscore + 1
          this.toggle = ""
        }
      }
    }
  }
})

new Vue({
  el: '#app',
  data: {
    comments: [
      {
        comment: 'komentar 1',
        date: '28-07-2020',
        score: 0
      },
      {
        comment: 'komentar 2',
        date: '28-07-2020',
        score: 4
      },
      {
        comment: 'komentar 3',
        date: '28-07-2020',
        score: 5
      },
    ],
    textarea: ""
  },
  methods: {
    addComments: function () {
      if (this.textarea !== "") {
        this.comments.push({
          comment: this.textarea,
          date: new Date().toLocaleString(),
          score: 0
        })
        this.textarea = ""
      }
    }
  },
})